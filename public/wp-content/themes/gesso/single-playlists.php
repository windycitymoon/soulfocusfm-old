<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

 $playlist_args = array(
   'post_type' => 'playlists',
   'posts_per_page' => -1,
   'order' => 'DESC'
 );


$context = Timber::get_context();
$timberpost = Timber::query_post();
$context['post'] = $timberpost;
$context['comment_form'] = TimberHelper::get_comment_form();
$context['pagination'] = Timber::get_pagination();
$context['playlist'] = Timber::get_posts($playlist_args);
$context['author'] = $timberpost->get_field('playlist_cuerator');
$context['playlist_moods'] = $timberpost->get_field('playlist_moods');
$context['playlist_genres'] = $timberpost->get_field('playlist_genres');
$context['oembed'] = $timberpost->get_field('playlist_playlist');
$tracks = $timberpost->playlist_tracks;


// Only load if Playlist
if( $timberpost->get_field('playlist_select') == 'Tracks') {



  // Playlist Term Collection: Read each track to get terms
  $mood_array = array();
  $genre_array = array();
  foreach($tracks as $track){
    $mood_terms = get_the_terms($track, 'moods');
    $genre_terms = get_the_terms($track, 'genres');

    // Collect all track mood terms
    if (is_array($mood_terms) || is_object($mood_terms)){
      foreach($mood_terms as $term){
        if(!in_array($term->name, $mood_array)){
          $mood_array[]=$term->name;
        }
      }
    }
    // Collect all track genre terms
    if (is_array($genre_terms) || is_object($genre_terms)){
      foreach($genre_terms as $gterm){
        if(!in_array($gterm->name, $genre_array)){
          $genre_array[]=$gterm->name;
        }
      }
    }
  }
  // Sort Terms Arrays
  asort($mood_array);
  asort($genre_array);

  $context['all_moods'] = $mood_array;
  $context['all_genres'] = $genre_array;
}



if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->post_type . '-' . $post->slug . '.twig', 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
