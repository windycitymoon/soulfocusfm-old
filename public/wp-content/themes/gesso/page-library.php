<?php
/* Template Name: Library Page */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$artists_args = array(
  'taxonomy' => 'artists',
  'orderby'  =>  'name',
  'order'  =>  'ASC'
);
$staff_args = array(
  'post_type' => 'staff',
  'posts_per_page' => 5,
  'order' => 'rand'
);
$playlist_args = array(
  'post_type' => 'playlists',
  'posts_per_page' => 5,
  'order' => 'ASC'
);
$event_args = array(
  'post_type' => 'events',
  'posts_per_page' => 5,
  'order' => 'ASC'
);
$moods_args = array(
  'taxonomy' => 'moods',
  'orderby'  =>  'name',
  'order'  =>  'ASC'
);
$genres_args = array(
  'taxonomy' =>  'genres',
  'orderby'  =>  'name',
  'order'  =>  'ASC'
);

$context['artists'] = Timber::get_terms($artists_args);
$context['staff'] = Timber::get_posts($staff_args);
$context['playlists'] = Timber::get_posts($playlist_args);
$context['events'] = Timber::get_posts($event_args);
$context['moods'] = Timber::get_terms($moods_args);
$context['genres'] = Timber::get_terms($genres_args);

Timber::render( array( 'page-' . $post->post_name . '.twig', 'page-' . $post->ID . '.twig', 'page.twig' ), $context );
