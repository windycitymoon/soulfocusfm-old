// Playlist page auto player

jQuery(document).ready(function($) {
  'use strict';
  var videoIDs = [];
  var playlistItem = $( ".playlist__tracklist-item" );
  // var trackIndex = $('.playlist__tracklist-item').index(this);
  // var trackDiv = $('playlist__track').index(this);
  var regEx = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;

  var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  $(".playlist__url").each(function() {
    var url = $(this).text();
    var result = url.match(regEx);
    videoIDs.push(result[2]);
    var data = $(this).attr('data-YT-id', result[2]);
  });

  var player,
      currentVideoId,
      playlistLength = videoIDs.length;

  window.onYouTubeIframeAPIReady = function() {
    player = new YT.Player("player", {
      height: "640",
      width: "100%",
      playerVars: {
        autoplay: 0,
        modestbranding: 1,
        enablejsapi: 1,
        fs: 0,
        playsinline: 1
      },
      events: {
        onReady: onPlayerReady,
        onStateChange: onPlayerStateChange,
        onError: onPlayerError
      }
    });
  }

  function onPlayerReady(event) {
    currentVideoId = 0;
    event.target.setVolume(100);
    event.target.loadVideoById(videoIDs[currentVideoId]);
  }

  function onPlayerError(event) {
    currentVideoId++;
    player.loadVideoById(videoIDs[currentVideoId]);
  }

  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
      playlistItem.not( currentVideoId ).removeClass('is-playing');
      playlistItem.eq( currentVideoId ).addClass('is-playing');
    }
    if (event.data == YT.PlayerState.ENDED) {
      currentVideoId++;
      if (currentVideoId < videoIDs.length) {
        player.loadVideoById(videoIDs[currentVideoId]);
      }
    }
  }

  $('#next').on('click', function(event) {
    if (currentVideoId < videoIDs.length - 1) {
      currentVideoId++;
      player.loadVideoById(videoIDs[currentVideoId]);
    }
    switch(true) {
      case (currentVideoId == videoIDs.length - 1 ):
        $(this).addClass('is-hidden');
        break;
      default:
        $('#prev').removeClass('is-hidden');
        $('.playlist__button--pause').removeClass('is-paused').html('PAUSE');
    }
  });

  $('#prev').on('click', function(event) {
    if (currentVideoId > 0) {
      currentVideoId--;
      player.loadVideoById(videoIDs[currentVideoId]);
    }
    switch(true) {
      case (currentVideoId == 0):
        $(this).addClass('is-hidden');
        break;
      default:
        $('#next').removeClass('is-hidden');
        $('.playlist__button--pause').removeClass('is-paused').html('PAUSE');
    }
  });

  $('#pause').on('click', function(){
    switch (player.getPlayerState()) {
      case 2:
        player.playVideo();
        $(this).html('PAUSE');
        $('.playlist__button--pause').removeClass('is-paused');
        break;
      default:
        player.pauseVideo();
        $(this).html('PLAY');
        $('.playlist__button--pause').addClass('is-paused');
    }
  });

  $('.playlist__tracklist-title').on('click', function(){
    $(this).toggleClass('is-hidden');
    $(this).hasClass('is-hidden') ? $(this).html('Hide Playlist') : $(this).html('View Playlist');
  });

  $('.playlist__tracklist-title').on('click', function(){
    $('.playlist__tracklist-list').slideToggle('slow');
  });

  // Allow user to click a playlist track and have the click track become the active track
  playlistItem.on('click', function(){
    $('#prev').removeClass('is-hidden');
    player.stopVideo();
    currentVideoId = playlistItem.index(this);
    player.loadVideoById(videoIDs[currentVideoId]);
  });


});
