// Custom scripts file

jQuery(document).ready(function($) {

  'use strict';
  var click = 0;
  $('#hero__toggle').on('click', function() {
    click++;
    if (click % 2 == 1) {
      $(this).html("[ Show notes + ]");
      $('.hero__description').slideToggle('slow').removeClass('is-hidden');
    } else {
      $(this).html("[ Hide notes - ]");
      $('.hero__description').slideToggle('slow').addClass('is-hidden');
    }
  });


  $('.hero__slide-container').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 5000,
    speed: 800,
    fade: true,
    appendArrows: '.nav-arrows',
    prevArrow: '<span class="nav-arrow nav-arrow--prev"><img src="/wp-content/themes/gesso/images/arrow-left.png" alt="Prev"/></span>',
    nextArrow: '<span class="nav-arrow nav-arrow--next"><img src="/wp-content/themes/gesso/images/arrow-right.png" alt="Next"/></span>',
    cssEase: 'linear'
  });

  $(".accordion__item-title:hidden").first().next().toggle(function() {

  });

  $('.facet__header').on('click', function(){
    $(this).toggleClass('is-up');
    $('.facet__window').toggleClass('is-open');
  });
  if ($(window).width() < 768) {
    $('.facet__window').removeClass('is-open');
  }

  $(".accordion__item-title").click(function() {
    var plus_icon = $(this).find('.fa-plus');
    var minus_icon = $(this).find('.fa-minus');
    var icon_fa_icon = plus_icon.attr('data-icon');
    var minus_icon_fa_icon = minus_icon.attr('data-icon');

    if (icon_fa_icon === "plus") {
      plus_icon.attr('data-icon', 'minus');
    }
    if (minus_icon_fa_icon === 'minus') {
      minus_icon.attr('data-icon', 'plus');
    }
    $(this).next().slideToggle("slow", function() {
      // Animation complete.
    });
  });

  $(".accordion__item-title").on("hover", function() {
    $(this).next().slideToggle("slow");
  });

// Generic function that runs on window resize.
  function resizeStuff() {}


  // Runs function once on window resize.
  var TO = false;
  $(window).resize(function() {
    if (TO !== false) {
      clearTimeout(TO);
    }

    // 200 is time in miliseconds.
    TO = setTimeout(resizeStuff, 200);
  }).resize();

});
