<?php
/* Template Name: Homepage Template */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$playlist_args = array(
  'post_type' => 'playlists',
  'posts_per_page' => -1,
  'order' => 'DESC'
);
$tracks_args = array(
  'post_type' => 'tracks',
  'posts_per_page' => -1,
  'order' => 'DESC',
  'meta_value' => '1'
);
$events_args = array(
  'post_type' => 'events',
  'posts_per_page' => -1,
  'order' => 'DESC'
);

$staff_args = array(
  'post_type' => 'staff',
  'posts_per_page' => 5,
  'order' => 'rand'
);
$genres_args = array(
  'taxonomy' =>  'genres',
  'orderby'  =>  'name',
  'order'  =>  'ASC'
);
$moods_args = array(
  'taxonomy' =>  'moods',
  'orderby'  =>  'name',
  'order'  =>  'ASC'
);

$context['playlists'] = Timber::get_posts($playlist_args);
$context['tracks'] = Timber::get_posts($tracks_args);
$context['staff'] = Timber::get_posts($staff_args);
$context['events'] = Timber::get_posts($events_args);
$context['genres'] = Timber::get_terms($genres_args);
$context['moods'] = Timber::get_terms($moods_args);

// Define generic templates.
$templates = array(
	'page-' . $post->post_name . '.twig',
	'page-' . $post->ID . '.twig',
	'page.twig'
);

// Set the Homepage template.
if ( is_front_page() ) array_unshift( $templates, 'front-page-2019.twig' );

// Render twig template.
if ( post_password_required( $post->ID ) ) {
	Timber::render( 'components/password-form.twig', $context );
} else {
	Timber::render( $templates, $context );
}
