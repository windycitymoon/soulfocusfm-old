<?php
/* Template Name: Playlist Landing Page */

global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$playlist_args = array(
  'post_type' => array('playlists', 'podcast'),
  'posts_per_page' => -1,
  'order' => 'DESC',
  'paged' => $paged,
  'facetwp' => true,
);
// $event_args = array(
//   'post_type' => 'events',
//   'posts_per_page' => 2,
//   'order' => 'DESC'
// );

$context['facet'] = new Timber\PostQuery($playlist_args);
// $context['facet'] = new WP_Query($playlist_args);
// $context['events'] = new Timber\PostQuery($event_args);


Timber::render( array( 'page-playlist.twig', 'page-' . $post->post_name . '.twig', 'page-' . $post->ID . '.twig', 'page.twig' ), $context );
