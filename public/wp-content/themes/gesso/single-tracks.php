<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context['post'] = new Timber\Post();

$oembed = $post->tracks_video;
$context['oembed'] = wp_oembed_get($oembed);
$context['pagination'] = Timber::get_pagination();

if( is_singular('tracks') ) {
  wp_reset_query();
  $context['prev_page'] = previous_post_link( );
  $context['next_page'] = next_post_link( );
}

if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->post_type . '-' . $post->slug . '.twig', 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context );
}
